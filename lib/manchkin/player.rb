# require 'player_states/base'

module Manchkin
  class Player

    attr_reader :game, :hand, :level, :power, :name, :id

    def initialize(name)
      @id = object_id
      @name = name
      @level = 1
      @power = 1
      @hand = []
      @carried = []
      @equipped = []
      @game_id = nil
      @state = ::Manchkin::PlayerReady.instance
    end

    # player's actions----------------------------------------

    def try_to_leave_monster(monster_card) # типа бросает кубик
      if can_perform_action? __method__
        @state.try_to_leave_monster self, monster_card
      end
    end

    def open_door
      if can_perform_action? __method__
        @state.open_door self
      end
    end

    def look_for_trouble(monster_card)
      if can_perform_action? __method__
        @state.look_for_trouble self, monster_card
      end
    end

    def search_room
      if can_perform_action? __method__
        @state.search_room self
      end
    end

    # //player's actions----------------------------------------

    def change_state(state)
      @state = state
    end

    def attach_to_game(game_id)
      @game_id = game_id
    end

    def game # вернет экземпляр игры
      ObjectSpace._id2ref @game_id
    end

    def add_cards(cards)
      if cards.is_a? Array
        @hand += cards
      else
        @hand.push cards
      end
    end

    private

    def can_perform_action?(method)
      @state.class.instance_methods(false).include? method
    end

    # def carry_gear(card)
    #   if card.is_a? Gear
    #     popped_card = pop_card_from(card, @equipped) || pop_card_from(card, @hand)
    #     @carried.push popped_card
    #   else
    #     # ошибку в лог что не шмотка
    #   end
    # end
    #
    # def equip_gear(card)
    #   if card.is_a? Gear
    #     popped_card = pop_card_from(card, @carried) || pop_card_from(card, @hand)
    #     @equipped.push popped_card
    #   else
    #     # ошибку в лог что не шмотка
    #   end
    # end
    #
    # private
    #
    # def pop_card_from(card, from) # from - @hand, @carried, @equipped; card - Card instance
    #   from.delete card
    # end
  end
end