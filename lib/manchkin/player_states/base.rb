require 'singleton'

module Manchkin
  class PlayerState
    include Singleton

  end

  class PlayerReady < PlayerState

  end

  class PlayerOpenDoor < PlayerState
    def open_door(player)
      p 'open_door'
      player.game.open_door player
    end
  end

  class PlayerOpenedDoor < PlayerState # если открыл дверь в открытую и не встретил монстра
    def look_for_trouble(player, monster_card)
      p 'look_for_trouble'
    end

    def search_room(player) #
      p 'search_room'
      player.game.search_room player
    end
  end

  class PlayerInBattle < PlayerState

  end

  class PlayerWinBattle < PlayerState

  end

  class PlayerTryToLeave < PlayerState
    def try_to_leave_monster(player, monster_card)
      player.game.try_to_leave_monster(player, monster_card)
    end
  end

  class PlayerLeftMonster < PlayerState

  end

  class PlayerSufferBadStuff < PlayerState

  end

end