module Manchkin

  class BattleCharacter
    attr_reader :power
  end

  class BattleMonster < BattleCharacter

    attr_reader :treasures_count, :state

    def initialize(card_monster)
      @monster = card_monster
      @attached_cards = []
      @power = @monster.level
      @treasures_count = @monster.treasures
      @state = :fit_as_fiddle # может быть :defeated, :killed
    end

    def attach_card(card)
      @attached_cards.push card
      recalculate_monster_attrs!
    end

    def defeated?
      state != :fit_as_fiddle
    end

    private

    def recalculate_monster_attrs!
      # @power
      # @treasures_count
    end

  end

  class BattlePlayer < BattleCharacter

    attr_reader :id, :role, :player, :treasures_count

    def initialize(player, role = :main)
      @id = object_id
      @role = role
      @attached_cards = []
      player.change_state ::Manchkin::PlayerInBattle.instance
      @player = player
      @power = @player.power
      @treasures_count = 0

      recalculate_player_power!
    end

    def attach_card(card)
      @attached_cards.push card

      recalculate_player_power!
    end

    private

    def recalculate_player_power!
      @power = @player.power
      # + attached card
    end

  end

  class Battle

    attr_reader :players, :monsters
    attr_accessor :winner

    def initialize(player, card_monster, logger)
      # @game = game
      @players = []
      @players.push ::Manchkin::BattlePlayer.new player
      @monsters = []
      @monsters.push ::Manchkin::BattleMonster.new card_monster

      @winner = nil
      @state = ::Manchkin::BattleInProgress.instance
      @logger = logger

      logger.push "начался бой между #{player.name} и <#{card_monster.id}>"
      logger.action :battle, owner: battle_owner, monsters: @monsters
    end

    def change_state(state)
      @state = state
    end

    # battle actions----------------------------------------

    def try_to_leave_monster(player, monster_card, number)
      if can_perform_action? __method__
        @state.try_to_leave_monster self, player, monster_card, number
      end
    end

    def end_battle
      if can_perform_action? __method__
        @state.end_battle self
      end
    end

    def add_monster(card_monster)
      if can_perform_action? __method__
        @state.add_monster self, card_monster
      end
    end

    def add_helper(player)
      if can_perform_action? __method__
        @state.add_monster self, player
      end
    end

    # //battle actions----------------------------------------


    def current_winner # :manchkin or :monster
      players_power = @players.inject(0) { |sum, item| sum + item.power }
      monsters_power = @monsters.inject(0) { |sum, monster| sum + monster.power unless monster.defeated? }

      return :manchkin if players_power > monsters_power
      :monster
    end

    def battle_owner
      @players.find { |pl| pl.role == :main }
    end

    def battle_helper
      @players.find { |pl| pl.role == :helper }
    end

    def get_monster_by_monster_card(monster_card)
      @monsters.find { |monster_char| monster_char.monster == monster_card }
    end

    def get_player_char_by_player(player)
      @players.find { |player_char| player_char.player == player }
    end

    private

    def can_perform_action?(method)
      @state.class.instance_methods(false).include? method
    end

  end
end