# EXTENSIONS = {
#     0 => { deck: :classic, name: 'Classic game' },
#     1 => { deck: :unnatural_axe, name: 'Unnutural axe' },
#     2 => { deck: :clerical_errors, name: 'Clerical errors' }
# }
#
# class NonSupportedExtension < StandardError
#   def initialize(message)
#     super "Extension #{message.to_s} not supported"
#   end
# end
#
# @cards = []
#
# def card
#   yield
#   @cards.push '1'
# end
#
# def load_cards(version = :classic)
#   current_version = EXTENSIONS.detect { |k, v| v[:deck] == version }
#   version_number = current_version.nil? ? (raise NonSupportedExtension, version) : current_version.first
#   (0..version_number).each do |num|
#     self.send :require, EXTENSIONS[num][:deck].to_s
#   end
# end
#
#
module Manchkin
  class Deck

    attr_reader :doors, :treasures, :cards

    #TODO вынести #define куданить подальше
    @tmp_cards = []

    def self.define(deck_name, previos_exetention = nil, &block)

      definition_proxy = DefinitionProxy.new
      definition_proxy.instance_eval(&block)

      unless previos_exetention.nil?
        @tmp_cards += definition_proxy.cards
        require "manchkin/decks/#{previos_exetention.to_s}"
      else
        Manchkin.loaded_cards = @tmp_cards + definition_proxy.cards
      end
    end

    def initialize(cards)
      @cards = cards
      @doors, @treasures = cards.partition { |card| card.is_a? Door }
      @doors_discard = []
      @treasures_discard = []
    end

    def shuffle!
      @doors.shuffle!
      @treasures.shuffle!
    end

    def draw_start_hand
      [
          draw_card,
          draw_card,
          draw_card(:treasure),
          draw_card(:treasure)
      ]
    end

    def draw_card(*args)
      deck = args.select { |i| i == :door || i == :treasure }.first || :door
      case deck
      when :door
        return @doors.shift
      when :treasure
        return @treasures.shift
      end
    end

  end



  class DefinitionProxy

    attr_reader :cards

    def initialize
      @cards = []
    end

    def card(card_class, &block)
      card_factory = CardFactory.new
      if block_given?
        card_factory.instance_eval &block
      end
      card_instance = Object.const_get("::Manchkin::#{card_class.capitalize}").allocate

      card_factory.attributes.each do |attr_name, attr_val|
        card_instance.instance_variable_set :"@#{attr_name}", attr_val
        card_instance.class.class_eval { send :attr_reader, :"#{attr_name}" }
      end

      card_instance.instance_variable_set :@card_type, card_instance.card_type
      card_instance.instance_variable_set :@subtype, card_instance.subtype
      card_instance.instance_variable_set :@id, card_instance.object_id
      # card_instance.class.class_eval { send :attr_reader, :card_type, :subtype }
      # card_instance.class.class_eval { send :attr_reader, :subtype }


      @cards.push card_instance
    end

  end

  class CardFactory < BasicObject
    def initialize
      @attributes = {}
    end

    attr_reader :attributes

    def method_missing(name, *args, &block)
      attributes[name] = args[0]
    end
  end
end