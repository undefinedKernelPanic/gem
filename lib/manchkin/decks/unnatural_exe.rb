Manchkin::Deck.define :unnatural_exe, :classic do
  card Monster do
    p 'monster form unn'
  end

  card Gear do
    p 'gear form unn'
  end
end
