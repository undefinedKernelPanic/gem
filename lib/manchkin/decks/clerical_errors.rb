Manchkin::Deck.define :clerical_errors, :unnatural_exe do
  card Monster do
    p 'monster form cleric'
  end

  card Gear do
    p 'gear form cleric'
  end
end
