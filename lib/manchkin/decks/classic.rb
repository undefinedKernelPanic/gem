Manchkin::Deck.define :classic do
  card :monster do
    level 3
    name 'aaa'
    treasures 3
  end

  card :monster do
    level 5
    name 'dd'
    restrictions -> (a) {p a}
    treasures 3
  end

  card :monster do
    level 2
    name 'aa23a'
    treasures 3
  end

  card :monster do
    level 12
    name '32'
    treasures 3
  end

  card :monster do
    level 4
    name 'aa23a'
    treasures 3
  end

  card :monster do
    level 12
    name 'aa23a'
    treasures 3
  end

  card :monster do
    level 112
    name '2'
    treasures 3
  end

  card :monster do
    level 121
    name 'ss'
    treasures 3
  end

  card :monster do
    level 112
    name '2'
    treasures 3
  end

  card :monster do
    level 121
    name 'ss'
    treasures 3
  end

  card :gear do
    bonus 5
  end

  card :gear do
    bonus 1
  end

  card :gear do
    bonus 5
  end
  card :gear do
    bonus 5
  end

  card :gear do
    bonus 1
  end

  card :gear do
    bonus 5
  end
  card :gear do
    bonus 5
  end

  card :gear do
    bonus 1
  end

  card :gear do
    bonus 5
  end
  card :gear do
    bonus 5
  end

  card :gear do
    bonus 1
  end

  card :gear do
    bonus 5
  end
end