# require 'decks/imp'
module Manchkin
  class Game

    # class Dice
    #   def initialize
    #     @dice = Random.new
    #   end
    #
    #   def throw
    #     @dice.rand 1..6
    #   end
    # end
    attr_reader :current_player, :players, :id, :deck, :logger, :battle, :state

    def initialize(deck, players, logger)
      @logger = logger || Manchkin::Logger.new
      @deck = deck
      @players = players
      @current_player = nil
      @dice = Random.new
      @battle = nil
      @id = self.object_id

      @state = :process
    end

    def cards
      @deck.cards
    end

    def start_game
      @deck.shuffle!

      @players.each do |p|
        p.add_cards @deck.draw_start_hand
        p.attach_to_game @id
      end

      # @current_player = @players[@dice.rand 0...@players.size]
      @current_player = @players.first
      @current_player.change_state ::Manchkin::PlayerOpenDoor.instance

      @logger.push 'game start'
      other_players.each { |player| @logger.push "сейчас ходит #{@current_player.name}", player.id }
      @logger.push 'твой ход', @current_player.id
      # @state = :ready
    end

    # ----------------------------------------
    def search_room(player)
      card = @deck.draw_card
      # брал в закрытую
      player.add_cards card
      next_turn!

    end

    def open_door(player)
      card = @deck.draw_card

      other_players.each { |player| @logger.push "#{@current_player.name} вытянул <#{card.id}>", player.id }
      @logger.push "тебе досталось <#{card.id}>", @current_player.id

      if card.is_a? Monster
        @battle = ::Manchkin::Battle.new @current_player, card, @logger
        @state = :battle
      else

        if card.is_a? Curse
          # проклятия и все дела
        else

          player.add_cards card
          player.change_state ::Manchkin::PlayerOpenedDoor.instance
        end
      end

    end

    def try_to_leave_monster(player, monster_card)
      # надо обработать все плюшки и штрафы на бросок
      @dice.rand 1..6
    end


    def next_player(player)
      players_count = @players.size
      index = @players.index player
      @players[(index + 1) % players_count]
    end

    def previous_player(player)
      index = @players.index player
      @players[index - 1]
    end

    private

    def other_players # все кроме того чей сча ход
      @players.select { |player| player != @current_player }
    end

    def next_turn!
      @current_player.change_state ::Manchkin::PlayerReady.instance
      @current_player = next_player @current_player
      @current_player.change_state ::Manchkin::PlayerOpenDoor.instance
    end


  end
end