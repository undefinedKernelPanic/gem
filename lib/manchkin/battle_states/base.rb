require 'singleton'

module Manchkin
  class BattleState
    include Singleton

  end

  class BattleInProgress < BattleState

    def end_battle(battle)
      battle.winner = battle.current_winner
      if battle.winner == :manchkin
        battle.change_state ::Manchkin::BattlePlayersWin.instance
        battle.players.each { |character| character.player.change_state Manchkin::PlayerWinBattle.instance }
        battle.battle_owner.treasures_count = @monsters.inject(0) { |sum, item| sum + item.treasures_count }
      else
        battle.change_state ::Manchkin::BattlePlayersTryLeave.instance
        battle.players.each { |character| character.player.change_state Manchkin::PlayerTryToLeave.instance }
      end
    end

    def add_monster(battle, card_monster)
      battle.monsters.push Manchkin::BattleMonster.new card_monster
    end

    def add_helper(battle, player)
      battle.players.push Manchkin::BattlePlayer.new player, :helper
    end

  end

  class BattlePlayersWin

  end


  class BattlePlayersTryLeave < BattleState
    def try_to_leave_monster(battle, player, monster_card, number)
      monster_char = battle.get_monster_by_monster_card monster_card
      unless monster_char.defeated?
        if (5..6).include? number
          # не убёг))
          player.change_state ::Manchkin::PlayerSufferBadStuff.instance
          # надо к игроку применить непотребство монстра
        else
          player.change_state ::Manchkin::PlayerLeftMonster.instance
          # убежал, но надо проверить мож монстр както навредил
        end
      end
    end
  end

end