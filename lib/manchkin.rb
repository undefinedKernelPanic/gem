require "manchkin/version"
require "manchkin/card"
require "manchkin/deck"
require "manchkin/game"
require "manchkin/battle_states/base"
require "manchkin/player_states/base"
require "manchkin/player"
require "manchkin/battle"

require "manchkin/logger"


module Manchkin

  class << self
    attr_accessor :loaded_cards
  end

  @loaded_cards = []

  def self.start_game(players, logger = nil, version = :classic)
    load_cards version
    deck = Deck.new @loaded_cards
    game = Game.new deck, players, logger
    game.start_game
    game
  end

  private

  def self.load_cards version
    require "manchkin/decks/#{version.to_s}"
  end
end

